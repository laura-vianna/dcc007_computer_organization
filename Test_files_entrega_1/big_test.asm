.data
array:.word   0 : 31         # "array" of words
.text


la $s1, array       # load address of array (ignore in mips)

addi $t1, $zero, 3
sw   $t1, 8($s1)
addi $t2, $zero, 1
loop: beq $t1, $zero, exit_loop
sub $t1, $t1, $t2
j loop
exit_loop: addi $t3, $t1, 4
add $t4,$t2,$t1
lw $t5, 8($s1)

