

module pipeline(

	input		clk,
	input		rst,
	output[7:0]		out_valor1,
	output[7:0]		out_valor2,
	output[7:0]		out_valor3
);


reg[7:0] reg_valor1;
reg[7:0] reg_valor2;
reg[7:0] reg_valor3;

reg[7:0] PC;
wire [7:0] out_rom;

reg halt;

	rom rom_1 (.address(PC),.clock(clk),.q(out_rom));
	
		
	assign out_valor1 = reg_valor1;
	assign out_valor2 = reg_valor2;
	assign out_valor3 = reg_valor3;
	
	
	
	always@(posedge clk)begin
	
		if(rst == 1'b1)
		begin
			reg_valor1 <= 8'b0;
			PC <= 8'b0;
			halt <= 1'b1;
		end
		
		else
		begin
			
			if(halt == 1'b1)
			begin
				halt <= 1'b0;
				PC <= PC + 1;
			end
			
			else
			begin
				PC <= PC + 1;
				reg_valor1 <= out_rom;
			end
		end
		
	end

	always@(posedge clk)begin
	
		if(rst == 1'b1)
		begin
			reg_valor2 <= 8'b0;
		end
		
		else
		begin
			
			reg_valor2 <= reg_valor1;
		end
		
	end
	
	always@(posedge clk)begin
	
		if(rst == 1'b1)
		begin
			reg_valor3 <= 8'b0;
		end
		
		else
		begin
			
			reg_valor3 <= reg_valor2;
		end
		
	end
	
	
endmodule