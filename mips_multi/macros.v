/*---------- Operation Codes ---------- */
`define	    OPCODE_R			6'b000000
`define	    R_OP_ADD			6'b100000
`define	    R_OP_SUB			6'b100010

`define	    I_OP_ADDI	    6'b001000
`define	    I_OP_BEQ	    6'b000100
`define	    I_OP_LW		    6'b100011
`define	    I_OP_SW		    6'b101011

`define	    J_OP_JUMP	    6'b000010

`define	    OPCODE_NOP    6'b001001


/*---------- Instructions ---------- */
`define	    INST_ADD			8'b00000001
`define	    INST_ADDI			8'b00000010
`define	    INST_SUB			8'b00000011
`define	    INST_BEQ			8'b00000100
`define	    INST_JUMP			8'b00000101
`define	    INST_LW				8'b00000110
`define	    INST_SW				8'b00000111
