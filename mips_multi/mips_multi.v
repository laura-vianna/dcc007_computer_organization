`include "macros.v"

module mips_multi(

	input				CLOCK_50,// Para a placa
	input[3:0]		KEY, // Para a placa
	input[17:0]		SW, // Para a placa
	output[8:0]		LEDG, // Para a placa
	output[0:6]		HEX0, // Para a placa
	output[0:6]		HEX1, // Para a placa
	output[0:6]		HEX2, // Para a placa
	output[0:6]		HEX3, // Para a placa
	output[0:6]		HEX4, // Para a placa
	output[0:6]		HEX5, // Para a placa
	output[0:6]		HEX6, // Para a placa
	output[0:6]		HEX7 // Para a placa

);

/*---------- Globais --------------- */
reg [31:0] clk; // contador utilizado para deixar a frequência do clock na placa mais lento
reg [9:0] PC; // Contador de programa do MIPS
wire is_jmp_or_beq; //Identifica instrucoes do tipo BEQ/JUMP que serao executadas
reg block_fetch; //Identifica quando deve matar instrucoes erradas
wire [31:0] target_PC; //PC de destino em BEQ/JUMP
wire [31:0] out_mem_inst; //saída da memória de instruções
wire [31:0] out_mem_data; //saída da memória de dados
wire signal_wren; 		 //write enable em memoria
wire [4:0] signal_rd; // utilizado para "criar" o multiplexador que seleciona o registrador de destino do MIPS
wire [31:0] signal_dado_a_ser_escrito; // utilizado para "criar" o multiplexador que seleciona qual dado será salvo no banco de registradores do MIPS
wire [31:0] dado_lido_1; // dado lido do banco de registardores
wire [31:0] dado_lido_2; // dado lido do banco de registardores
wire [31:0] signal_reg_para_a_placa; // para a placa
/*---------- Globais --------------- */

/*---------- IF = Fetch ---------- */
reg [31:0] FD_IR; // Fetch: Registrador de instrução do MIPS
reg [9:0] FD_PC; // Registrador que carrega o PC (usado no beq para calcular desvio)
/*---------- IF = Fetch ---------- */

/*---------- ID = Decode ---------- */
reg [31:0] DE_immediate; // Imediato da instrução
reg [31:0] DE_A; // Registrador A do MIPS
reg [31:0] DE_B; // Registrador B do MIPS
reg [7:0] DE_FSM2; // Decode: controle das instruções do MIPS
reg [4:0] DE_RD;		//Registrador destino
reg [9:0] DE_PC; // Registrador que carrega o PC (usado no beq para calcular desvio)
/*---------- ID = Decode ---------- */

/*---------- EX = Execute ---------- */
reg [31:0] EM_saida_ula;	//registrador de saída da ULA do MIPS
reg [7:0] EM_FSM2;			//Controle das instruções
reg [4:0] EM_RD;				//Registrador Destino
reg [9:0] EM_PC;				// Registrador que carrega o PC
reg [31:0] EM_dado_a_ser_escrito;	//Dado que sera escrito no BR
/*---------- EX = Execute ---------- */

/*---------- Memory ---------------- */
reg [31:0] MW_saida_ula;	//registrador de saida da ula
reg [7:0] MW_FSM2;			//Controle das instruções
reg [4:0] MW_RD;				//Registrador Destino
wire [31:0] MW_dado_a_ser_escrito;
/*---------- Memory ---------------- */

/*---------- Writeback ---------------- */
wire br_wen;	//write enable do banco de registradores
wire reset;		//RESET
/*---------- Writeback ---------------- */

	mem_inst mem_i(.address(PC),.clock(clk[25]),.q(out_mem_inst)); // instanciando a memória de instruções (ROM)

	mem_data mem_d(.address(EM_saida_ula[9:0]), .clock(clk[25]), .data(DE_B), .wren(signal_wren), .q(out_mem_data)); // instanciando a memória de dados (RAM)
									 //para a placa		  para a placa
	banco_de_registradores br(.br_in_SW(SW[4:0]), .br_out_reg_para_a_placa(signal_reg_para_a_placa), .br_in_clk(clk[25]), .br_in_wen(br_wen), .br_in_reset(reset), .br_in_rs(FD_IR[25:21]), .br_in_rt(FD_IR[20:16]), .br_in_rd(signal_rd), .br_in_data(MW_dado_a_ser_escrito), .br_out_R_rs(dado_lido_1), .br_out_R_rt(dado_lido_2)); // // instanciando o banco de registardores

	/*---------- para a placa ---------- */		
	displayDecoder DP7_0(.entrada(signal_reg_para_a_placa[3:0]),.saida(HEX0)); 
	displayDecoder DP7_1(.entrada(signal_reg_para_a_placa[7:4]),.saida(HEX1)); 
	// utilizando o LEDG[0] para exibir o bit 25 do contador clk (para ter noção da frequência do clock utilizado)
	assign LEDG[0] = clk[25]; 
	/*---------- para a placa ---------- */
	
	
	
		/*---------- SB = Scoreboarding ---------- */
		
		//32 linhas (registradores) com 4 colunas: pending(3), is_LW?(2), EX_MEM(1), MEM_WB(0)	
		reg [3:0] reg_status [0:31];
		//contador para iteracoes (32 = 2^5)
		reg [5:0] i;
		wire [31:0] forward_1;
		wire [31:0] forward_2; 
		wire sb_stall;

	
	//no decode, ver se a instrucao em Exec e um LW. Verificar se o destino da LW e um dos operandos
	//da instrucao em decode, se sim. Stall.
	//no decode. if operandos 100, prox ciclo os operandos 010. Manda ordem de encaminhamento no execute 
	
	//if is_LW? e EX_MEM, da um stall. if is_LW? e MEM_WB faz encaminhamento.
	//matrix 2d verilog
	//if lw atrasar o pendente. valido pra encaminhamento so entre MEM e WB
	
	//wire 32 bits inA
	//wire 32 bits inB
	//.br_in_rs(FD_IR[25:21]), .br_in_rt(FD_IR[20:16])
	
	
	
	//caso contrario, o dado esta no banco de registradores (dado_lido_1 e _2)

//	assign forward_1 = reg_status[FD_IR[25:21]][3] ? //se o dado A esta pendente 
//								(reg_status[FD_IR[25:21]][1] ? EM_saida_ula : //se o dado estiver em entre EX MEM, retorna a EM_saida_ula
//									(reg_status[FD_IR[25:21]][0] ? MW_saida_ula : //se o dado estiver entre MEM e WB retorna MW_saida_ula
//									dado_lido_1)) : //caso contrario, o dado esta no banco de registradores
//							 dado_lido_1; //caso nao esteja pendente, o dado esta no banco de registradores
	assign forward_1 = reg_status[FD_IR[25:21]][1] ? EM_saida_ula : (reg_status[FD_IR[25:21]][0] ? MW_saida_ula : dado_lido_1);
	assign forward_2 = reg_status[FD_IR[20:16]][1] ? EM_saida_ula : (reg_status[FD_IR[20:16]][0] ? MW_saida_ula : dado_lido_2);
	assign sb_stall = (reg_status[FD_IR[25:21]][2:1] == 2'b11 ) || (reg_status[FD_IR[20:16]][2:1] == 2'b11) ? 1'b1 : 1'b0;
	
	//assign forward_B = Rt.ExMem ? saidaUla : (Rt.MemWB ? MW_saida_ula : B);
	//Entrada da ula passa a ser inA e inB;
	
	//se a instrucao e do tipo LW, define o status is_LW?
	//assign wire_reg_status[FD_IR[20:16]][2] = (FD_IR[31:26] == `INST_LW) ? 1'b1 : 1'b0;
	
		/*---------- SB = Scoreboarding ---------- */

	
	


	// código que cria o multiplexador que seleciona o dado a ser ecrito no registrador de destino do MIPS
	//Se for load, recebe a saida da memoria, senão, a saida da ula.
	assign MW_dado_a_ser_escrito = ( (MW_FSM2 == `INST_LW) ) ? out_mem_data : MW_saida_ula;	

	// código que cria o multiplexador que seleciona o registrador de destino do MIPS
	assign signal_rd	=	MW_RD;
	
	//Se o estagio e Memory e a instrucao e store, o sinal de write enabled e um, caso contrario, 0	
	assign signal_wren = (EM_FSM2 == `INST_SW) ? 1'b1 : 1'b0;

	//Habilita escrita no banco de registradores
	assign br_wen = (MW_FSM2 == `INST_ADDI || MW_FSM2 == `INST_ADD ||
						  MW_FSM2 == `INST_SUB || MW_FSM2 == `INST_LW) ? 1'b1 : 1'b0;
		
	//Reseta valores no banco de registradores
	assign reset = (KEY[0] == 0) ?  1'b1 : 1'b0 ;
	
	//	Detecta se um JUMP/BEQ foi pro decode, preparando para a troca de PC
	assign is_jmp_or_beq = (FD_IR[31:26] == `J_OP_JUMP) || (EM_PC != 9'b0)  ? 1'b1 : 1'b0;
	
	//Usado pelo JUMP para saber o PC de destino
	assign target_PC =  (FD_IR[31:26] == `J_OP_JUMP) ? {{10{FD_IR[9]}}, FD_IR[9:0]} : EM_PC;
	
	
	// incrementado o contador clk em função do CLOCK_50 (clock de 50 Mhz interno da placa)
	always@(posedge CLOCK_50)begin
		clk = clk + 1;		
	end
	
	
	/*---------- IF = Fetch ---------- */
	always@(posedge clk[25])begin
		if(KEY[0] == 0)// Reset
		begin
			PC = 10'b0;
			FD_IR = 32'b0;
			block_fetch = 1'b0;
			
			//zera o status de todos os registradores no scoreboard
			//Não pode ser feita essa atribuição pois estaríamos fazendo
			//duas atribuições ao reg_status em dois always diferentes e
			//pq todos os registradores ficariam não pendentes o tempo todo.
			/*for (i=0; i<=31; i=i+1) begin
				//reg_status[i] = 4'b0;
			end*/
		end
		else begin
			if(is_jmp_or_beq == 1'b0)
			begin
				if(block_fetch == 1'b0)
				begin
					PC = PC + 1;
					FD_PC <= PC;
					FD_IR <= out_mem_inst;					
				end
				else begin
					block_fetch <= 1'b0;
				end
			end
			else begin
				PC <= target_PC;
				FD_IR <= 32'b0;
				block_fetch <= 1'b1;
			end
		end
	end
	/*---------- IF = Fetch ---------- */

	
	/*---------- ID = Decode ---------- */
	always@(posedge clk[25])begin
		if(KEY[0] == 0 || is_jmp_or_beq == 1'b1 || block_fetch == 1'b1)// Reset/kill op
		begin
			DE_FSM2 = 8'b0;
			DE_RD = 5'b0;
			DE_A = 32'b0;
			DE_B = 32'b0;
			DE_immediate = 32'b0;
		end
		else
			//Faz o shift right dos dois ultimos bits de status (de EX_MEM para MEM_WB)
			for (i=0; i<=31; i=i+1) begin
				reg_status[i][1:0]=reg_status[i][1:0]>>1;
				if(reg_status[i][1:0] == 2'b00) begin // Quando o dado nao esta em EX_MEM ou MEM_WB                    
					reg_status[i][3:2] = 2'b00; //O registrador nao esta mais pendente
				end
			end
				//Decodifica a instrucao		
				if(FD_IR[31:26] == `OPCODE_R) // add or sub
				begin
					if(FD_IR[5:0] == `R_OP_ADD) begin
						DE_FSM2 <= `INST_ADD;		//add			
					end			
					if(FD_IR[5:0] == `R_OP_SUB) begin
						DE_FSM2 <= `INST_SUB;		//sub		
					end
						DE_RD <= FD_IR[15:11];		//Registrador destino					
						reg_status[FD_IR[15:11]][3] <= 1'b1; //define o RD como pendente
						reg_status[FD_IR[15:11]][2] <= 1'b0;  //define o status is_LW?
						reg_status[FD_IR[15:11]][1:0] <= 2'b10;  

				end
			
				if(FD_IR[31:26] == `I_OP_ADDI)//addi
				begin
					DE_FSM2 <= `INST_ADDI;//addi
					DE_RD <= FD_IR[20:16];	//Registrador de destino
					reg_status[FD_IR[20:16]][3] <= 1'b1; //define o RD como pendente
					reg_status[FD_IR[20:16]][2] <= 1'b0;  //define o status is_LW?
					reg_status[FD_IR[20:16]][1:0] <= 2'b10;  //define o status is_LW?
				end
				if(FD_IR[31:26] == `I_OP_BEQ) // beq
				begin
					DE_FSM2 <= `INST_BEQ;//beq
				end
				if(FD_IR[31:26] == `J_OP_JUMP) // jump
				begin
					DE_FSM2 <= 8'b0;
					DE_RD <= 5'b0;
					DE_A <= 32'b0;
					DE_B <= 32'b0;
					DE_immediate <= 32'b0;
				end				
				if(FD_IR[31:26] == `I_OP_LW) // load
				begin
					DE_FSM2 <= `INST_LW;//load
					DE_RD <= FD_IR[20:16];	//Registrador de destino
					reg_status[FD_IR[20:16]][3] <= 1'b1; //define o RD como pendente
					reg_status[FD_IR[20:16]][2] <= 1'b1;  //define o status is_LW?
					reg_status[FD_IR[20:16]][1:0] <= 2'b10;
				end
				if(FD_IR[31:26] == `I_OP_SW) // store
				begin
					DE_FSM2 <= `INST_SW;//store
				end
				if(FD_IR == 32'b0)
				begin
					DE_FSM2 <= 8'b0;
				end
				
				DE_A <= forward_1;
				DE_B <= forward_2;
				DE_PC <= FD_PC;
				DE_immediate <= {{16{FD_IR[15]}}, FD_IR[15:0]};	
		end	
	/*---------- ID = Decode ---------- */
	
	
	/*---------- EX = Execute ---------- */
	always@(posedge clk[25])begin
		if(KEY[0] == 0)// Reset/kill op
		begin
			EM_saida_ula = 32'b0;
			EM_FSM2 = 32'b0;
			EM_RD = 5'b0;
			EM_PC = 10'b0;
			EM_dado_a_ser_escrito = 32'b0;
		end
		if(sb_stall == 1'b0) begin
		if(DE_FSM2 == `INST_ADD)// execute add
			begin
				EM_saida_ula <= DE_A + DE_B;
			end
			if(DE_FSM2 == `INST_ADDI)//execute addi
			begin
				EM_saida_ula <= DE_A + DE_immediate;
			end
			if(DE_FSM2 == `INST_SUB)// execute sub
			begin
				EM_saida_ula <= DE_A - DE_B;
			end
			if(DE_FSM2 == `INST_BEQ)// execute beq
			begin
				if (DE_A == DE_B)
				begin
					EM_PC <= DE_PC + DE_immediate;
				end				
			end
			else begin
				if(DE_FSM2 == `INST_JUMP)// execute jump
				begin
					EM_PC <= {{10{FD_IR[9]}}, FD_IR[9:0]};
				end	
				else begin
					EM_PC <= 9'b0;
				end
			end
			if(DE_FSM2 == `INST_LW)// execute load
			begin
				EM_saida_ula <= DE_A + DE_immediate;
			end
			if(DE_FSM2 == `INST_SW)// execute store
			begin
				EM_saida_ula <= DE_A + DE_immediate;
			end
			
			EM_FSM2 <= DE_FSM2;
			EM_RD <= DE_RD;		

			end
			else begin
			EM_FSM2 <= 32'b0;
			EM_RD <= 5'b0;
			end			
	
	end
	/*---------- EX = Execute ---------- */

	
	/*---------- MEM = Memory ---------- */
	always@(posedge clk[25])begin
		if(KEY[0] == 0)// Reset
		begin
			MW_saida_ula = 32'b0;
			MW_FSM2 = 32'b0;
			MW_RD = 5'b0;
		end
	
		MW_FSM2 <= EM_FSM2;
		MW_saida_ula <= EM_saida_ula;		
		MW_RD <= EM_RD;
	end
	/*---------- MEM = Memory ---------- */

	
	/*---------- WB = Write Back ---------- */
	always@(posedge clk[25])begin	
	end
	/*---------- WB = Write Back ---------- */

endmodule