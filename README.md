# dcc007_computer_organization

## Authors/Autores:  
https://github.com/JonanBH   
https://gitlab.com/laura-vianna   
https://github.com/NeliocmSampaio   


### Repository for group coursework of Computer Organization II module - 2017/1  

1. v1.0.0 - Simple MIPS - Verilog  
https://gitlab.com/laura-vianna/dcc007_computer_organization/tags/1.0.0

2. v2.0.0 - Simple pipelined MIPS Simples - Verilog  
https://gitlab.com/laura-vianna/dcc007_computer_organization/tags/2.0.0

3. v3.0.0 - Simple pipelined MIPS Simples with stall and forwarding - Verilog    
https://gitlab.com/laura-vianna/dcc007_computer_organization/tags/3.0.0   
(Does not work perfectly)  

4. v3.0.1 - Simple pipelined MIPS Simples with stall and forwarding - Verilog  
https://gitlab.com/laura-vianna/dcc007_computer_organization/merge_requests/17   
(Most updated version, there might be bugs)

5. v4.0.0 - Simple dual core MIPS - Verilog     
https://gitlab.com/laura-vianna/dcc007_computer_organization/merge_requests/14    
(Only uncomplete part of the project)


### Repositorio para trabalho em grupo da disciplina Organizacao de Computadores - 2017/1  

1. v1.0.0 - MIPS Simples - Verilog   
https://gitlab.com/laura-vianna/dcc007_computer_organization/tags/1.0.0

2. v2.0.0 - MIPS Simples com pipeline - Verilog      
https://gitlab.com/laura-vianna/dcc007_computer_organization/tags/2.0.0

3. v3.0.0 - MIPS Simples com pipeline, encaminhamento e stall - Verilog        
https://gitlab.com/laura-vianna/dcc007_computer_organization/tags/3.0.0   
(Nao funciona perfeitamente) 

4. v3.0.1 - MIPS Simples com pipeline, encaminhamento e stall - Verilog       
https://gitlab.com/laura-vianna/dcc007_computer_organization/merge_requests/17   
(Versao mais atual, podem haver bugs) 

5. v4.0.0 - MIPS simples dual core - Verilog      
https://gitlab.com/laura-vianna/dcc007_computer_organization/merge_requests/14   
(Unica parte nao finalizada do projeto)   
